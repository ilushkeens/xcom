(function ($) {
  'use strict'

  // Функция установки курсора в нужной позиции
  $.fn.setCursorPosition = function (pos) {
    if ($(this).get(0).setSelectionRange) {
      $(this).get(0).setSelectionRange(pos, pos)
    } else if ($(this).get(0).createTextRange) {
      let range = $(this).get(0).createTextRange()
      range.collapse(true)
      range.moveEnd('character', pos)
      range.moveStart('character', pos)
      range.select()
    }
  }

  // Добавляет маску к полю формы с телефоном
  $(window).on('load', function () {
    $('input[name=phone]').on('click', function () {
      $(this).setCursorPosition(4)
    }).mask('+7 (999) 999-99-99', { autoclear: false })
  })

  // Функция открытия popup-окна
  function openPopup(url) {
    $('#popup .overlay').fadeIn(300, function () {
      $('html').css({ 'overflow-y': 'hidden', 'overflow-x': 'hidden' })
      $('#popup, #popup .overlay').show()
      $(url).css('display', 'flex').animate({ opacity: 1 }, 300)
    })

    statusPopup = true
  }

  // Функция закрытия popup-окна
  function closePopup() {
    $('#popup .module').animate({ opacity: 0 }, 300, function () {
      history.pushState(null, document.title, window.location.pathname)

      $('#popup, #popup .overlay, #popup .module').hide()
      $('#popup .overlay').fadeOut(300)
      $('html').css('overflow-y', 'visible')
    })

    statusPopup = false
  }

  // Функция переключения popup-окон
  function togglePopup(url) {
    $('#popup .module').hide()
    $(url).css('display', 'flex').animate({ opacity: 1 }, 300)
  }

  // let hashes = ['become-a-supplier', 'success']
  let url,
      statusPopup = false

  // Открывает popup-окно
  $('.open-popup').on('click', function (event) {
    event.preventDefault()

    url = $(this).attr('href')

    openPopup(url)
    event.stopPropagation()
  })

  // ..при динамическом изменении страницы
  // window.addEventListener('hashchange', function (event) {
  //   url = decodeURI(window.location.hash)

  //   $.each(hashes, function (index, value) {
  //     if (url.replace('#', '') == value.toString()) togglePopup(url)
  //   })

  //   event.stopPropagation()
  // })

  // Закрывает popup-окно
  $('.close-popup').on('click', function (event) {
    event.preventDefault()
    closePopup()
  })

  // ..при нажатии на клавишу Escape
  $('body').on('keydown', function (event) {
    if (event.code == 'Escape') closePopup()
  })

  // Контролирует отправку форм
  $('form').each(function () {
    let form = $(this),
        input = form.find('textarea, input'),
        radio = form.find('.radio'),
        button = form.find('.button_send')

    // Функция проверки заполнения обязательных полей формы
    function checkInput() {
      input.each(function () {
        let field = $(this)

        // Проверяет обязательно ли поле к заполнению
        if (field.attr('required') !== undefined) {
          if (field.val() != '') {
            // Если поле не пустое удаляет класс-указание
            field.removeClass('empty')
          } else {
            // Если поле пустое добавляет класс-указание
            field.addClass('empty')
          }
        }
      })
    }

    // Функция подсветки незаполненных полей
    function lightEmpty() {
      // Добавляет подсветку
      form.find('.empty').addClass('error')

      // ..и через 1.5 секунды удаляет её
      setTimeout(function () {
        form.find('.empty').removeClass('error')
      }, 1500)
    }

    // Проверяет форму в режиме реального времени
    setInterval(function () {
      checkInput()

      // Считает количество незаполненных полей
      let countEmpty = form.find('.empty').length

      // Добавляет условие-тригер к кнопке отправки формы
      if (countEmpty > 0) {
        if (button.hasClass('button_disabled')) {
          return false
        } else {
          button.addClass('button_disabled')
        }
      } else {
        button.removeClass('button_disabled')
      }
    }, 1000)

    // Выбирает нужное поле для ввода контактов
    radio.on('click', function () {
      let target = $(`.contact_${ $(this).find('input[name=type]').val() }`)

      $('.contact').find('input').removeAttr('required').removeClass('empty')
      $('.contact').not(target).hide()
      target.find('input').attr('required', '')
      target.fadeIn(300)
    })

    // Обрабатывает попытку отправки формы
    button.on('click', 'a', function (event) {
      event.preventDefault()

      if (button.hasClass('button_disabled')) {
        // Подсвечивает незаполненные поля
        lightEmpty()
        return false
      } else {
        // Отправляет форму
        form.on('submit')
        input.val('')

        // Сообщает об успешной отправке
        if (statusPopup === true) {
          togglePopup('#success')
        } else {
          openPopup('#success')
        }
      }
    })
  })
}(jQuery))
