# XCOM change log

## Version 1.0.0 under development

- New: Added all page templates
- New: Added Montserrat font
- New: Added jQuery, jQuery.maskedinput
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init April 05, 2022
